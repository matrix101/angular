import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isVisible: boolean;
  clicks = [];
  /**
   *
   */
  constructor() {
    this.isVisible = true;
    this.clicks = [];
  }

  toggleVisibility() {
    this.isVisible = !this.isVisible
    this.onAddClick();
    return this.isVisible ? 'visible' : 'hidden';
  }

  getVisibility() {
    return this.isVisible;
  }

  onAddClick() {
    var item = { id: this.clicks.length, value: new Date() };
    console.log(item)
    this.clicks.push(item);
  }
}
