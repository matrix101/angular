import { Recipe } from './recipe.model';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class RecipeService {

  recipesChanged = new Subject<Recipe[]>();
  private recipes: Recipe[] = [];
  // private recipes: Recipe[] = [
  //   new Recipe(
  //     'Burger',
  //     'This is simply a burger',
  //     'https://1.bp.blogspot.com/-zhBZYNjps5A/Tyb6uBfLTYI/AAAAAAAADkU/tsskfgLg0IE/s1600/Best-top-desktop-food-wallpapers-hd-food-wallpaper-food-pictures-image-photo-2.jpg',
  //     [
  //       new Ingredient('Meat', 1),
  //       new Ingredient('Bread', 1),
  //       new Ingredient('Potatoes', 44)
  //     ]
  //   ),
  //   new Recipe(
  //     'Pizza',
  //     'This is simply a pizza',
  //     'https://2.bp.blogspot.com/-dV1rgtcLXZ8/Tyb6tHeahsI/AAAAAAAADkM/Y6qVQDuHxac/s1600/Best-top-desktop-food-wallpapers-hd-food-wallpaper-food-pictures-image-photo-1.jpg',
  //     [new Ingredient('Cheese', 1), new Ingredient('Tomato sauce', 2)]
  //   )
  // ];

  getRecipes() {
    return this.recipes.slice(); // returns a copy instead reference
  }
  getRecipe(id: number) {
    return this.recipes[id];
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe;
    this.recipesChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipesChanged.next(this.recipes.slice());
  }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }
}
