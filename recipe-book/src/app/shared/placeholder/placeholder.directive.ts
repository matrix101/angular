import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appPlaceholder]',
})

export class PlaceholderDirective {
  // gets the reference from where this directive is being used
  constructor(public viewContainerRef: ViewContainerRef) { }

}
