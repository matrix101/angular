import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Recipe } from '../recipes/recipe.model';
import { RecipeService } from '../recipes/recipe.service';
import { map, tap, take, exhaustMap } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { BehaviorSubject } from 'rxjs';
import { User } from '../auth/user.model';


@Injectable({ providedIn: 'root' })
export class DataStorageService {
  user = new BehaviorSubject<User>(null);

  constructor(private httpClient: HttpClient, private recipeService: RecipeService, private authService: AuthService) { }

  storeRecipes() {
    const recipes = this.recipeService.getRecipes();
    this.httpClient
      .put('https://recipebook-dde46.firebaseio.com/recipes.json', recipes)
      .subscribe(response => {
        console.log(response);
      });
  }

  fetchRecipes() {
    // take will unsubscrive once n element was recived (1)
    // exhaustMap
    return this.httpClient
      .get<Recipe[]>('https://recipebook-dde46.firebaseio.com/recipes.json')
      .pipe(
        map(recipes => {
          return recipes.map(recipe => { // get sure recipe has at least an empty array
            return { ...recipe, ingredients: recipe.ingredients ? recipe.ingredients : [] };
          });
        }), tap(recipes => {
          this.recipeService.setRecipes(recipes);
        }));
  }
}
