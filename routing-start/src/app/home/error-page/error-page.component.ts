import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Data } from "@angular/router";

@Component({
  templateUrl: "error-page.component.html"
})
export class ErrorPageComponent implements OnInit {
  errorMessage: string;

  ngOnInit() {
    // this.errorMessage = this.route.snapshot.data['message'];
    this.route.data.subscribe((data: Data) => {
      this.errorMessage = data["message"];
    });
  }
  constructor(private route: ActivatedRoute) {}
}
