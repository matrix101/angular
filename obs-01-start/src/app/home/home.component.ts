import { Component, OnInit, OnDestroy } from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  constructor() {}
  private obSubscription: Subscription;

  ngOnInit() {
    // this.obSubscription = interval(1000).subscribe(count => {
    //   console.log(count);
    // });

    const customIntervalOb = new Observable(observer => {
      let count = 0;
      setInterval(() => {
        observer.next(count);

        if (count === 2) {
          observer.complete();
        }
        if (count > 3) {
          observer.error(new Error('Counter is greater than 3'));
        }

        count++;
        // observer.complete();
        // observer.error();
      }, 1000);
    });

    this.obSubscription = customIntervalOb
      .pipe(
        filter((data: number) => {
          return data > 0;
        }),
        map((data: number) => {
          return 'Round: ' + (data + 1);
        })
      )
      .subscribe(
        count => {
          console.log(count);
        },
        error => {
          console.log(error);
          alert(error.message);
        },
        () => {
          // oncomplete THIS IS NOT EXECUTED IF ERROR IS THROWN
          console.log('Completed');
        }
      );
  }

  ngOnDestroy(): void {
    this.obSubscription.unsubscribe();
  }
}
