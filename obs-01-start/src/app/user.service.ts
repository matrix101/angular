import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserService {
  activedEmitter = new Subject<boolean>();
  // better performance than eventEmitter but its required to unsubscribe manually but cannot be used with @Output
  // activedEmitter = new EventEmitter<boolean>();
}
