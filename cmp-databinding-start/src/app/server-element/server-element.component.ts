import { Component, OnInit, Input, OnChanges, SimpleChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy, ViewChild, ElementRef, ContentChild } from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {
  @Input('srvElement') element: { type: string, name: string, content: string };
  @Input('') name: string;
  @ViewChild('heading', { static: true }) header: ElementRef;
  @ContentChild('contentParagraph', { static: true }) paragraph: ElementRef; 

    constructor() {
      console.log("constructor(9");
    }

  ngOnInit() {
    console.log("ngOnInit()");
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log("ngOnChanges()");
    console.log(changes);
  }
  ngDoCheck(): void {
    console.log("ngDoCheck()");
  }

  ngAfterContentInit(): void {
    console.log("ngAfterContentInit()");
    console.log(this.paragraph.nativeElement.textContent);
  }

  ngAfterContentChecked(): void {
    console.log("ngAfterContentChecked()");

  }
  ngAfterViewInit(): void {
    console.log("ngAfterViewInit()");
    console.log(this.header.nativeElement.textContent);
  }

  ngAfterViewChecked(): void {
    console.log("ngAfterViewChecked()");
  }
  ngOnDestroy(): void {
    console.log("ngOnDestroy()");
  }
}
